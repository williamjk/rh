class CreateAgencia < ActiveRecord::Migration
  def change
    create_table :agencia do |t|
      t.string :nome
      t.string :nomadministrador
      t.string :emailadministrador
      t.string :status
      t.integer :tipocontaagencia

      t.timestamps null: false
    end
  end
end
