class CreateCursos < ActiveRecord::Migration
  def change
    create_table :cursos do |t|
      t.integer :idCliente
      t.string :nomeInstituicao
      t.string :titulo
      t.date :dtInicio
      t.date :dtFinal

      t.timestamps null: false
    end
  end
end
