class CreateVagas < ActiveRecord::Migration
  def change
    create_table :vagas do |t|
      t.string :titulo
      t.text :descricao
      t.integer :empresa
      t.date :dtinicial
      t.date :dtfinal
      t.string :status

      t.timestamps null: false
    end
  end
end
