class CreateContaEmpresas < ActiveRecord::Migration
  def change
    create_table :conta_empresas do |t|
      t.float :valor
      t.string :formaPgto

      t.timestamps null: false
    end
  end
end
