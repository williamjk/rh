class CreateContaAgencia < ActiveRecord::Migration
  def change
    create_table :conta_agencia do |t|
      t.float :valor
      t.string :formaPgto

      t.timestamps null: false
    end
  end
end
