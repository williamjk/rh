class CreateClientes < ActiveRecord::Migration
  def change
    create_table :clientes do |t|
      t.string :nome
      t.string :email
      t.date :dtNasc
      t.text :resumo
      t.string :titulo

      t.timestamps null: false
    end
  end
end
