class CreateIdiomas < ActiveRecord::Migration
  def change
    create_table :idiomas do |t|
      t.integer :idCliente
      t.string :nomeInstituicao
      t.string :titulo
      t.string :nivel
      t.date :dtInicio
      t.date :dtFinal

      t.timestamps null: false
    end
  end
end
