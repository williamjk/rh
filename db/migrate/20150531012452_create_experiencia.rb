class CreateExperiencia < ActiveRecord::Migration
  def change
    create_table :experiencia do |t|
      t.integer :idCliente
      t.string :cargo
      t.integer :empresa
      t.date :dtInicial
      t.date :dtFinal
      t.text :descricao

      t.timestamps null: false
    end
  end
end
