class CreateTemporarios < ActiveRecord::Migration
  def change
    create_table :temporarios do |t|
      t.integer :idvaga
      t.integer :idcliente
      t.string :inscrito
      t.string :preselecionado
      t.string :efetivado

      t.timestamps null: false
    end
  end
end
