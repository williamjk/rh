class CreateFeedbacks < ActiveRecord::Migration
  def change
    create_table :feedbacks do |t|
      t.integer :idcliente
      t.integer :idusuario
      t.text :descricao
      t.integer :nota
      t.integer :idvaga

      t.timestamps null: false
    end
  end
end
