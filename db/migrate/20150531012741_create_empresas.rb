class CreateEmpresas < ActiveRecord::Migration
  def change
    create_table :empresas do |t|
      t.string :nome
      t.text :descricao
      t.string :site
      t.text :endereco
      t.string :tamanho
      t.string :nmresponsavelrh
      t.string :emailresponsavelrh
      t.integer :tipocontaempresa

      t.timestamps null: false
    end
  end
end
