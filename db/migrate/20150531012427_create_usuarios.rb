class CreateUsuarios < ActiveRecord::Migration
  def change
    create_table :usuarios do |t|
      t.integer :agencia
      t.integer :empresa
      t.string :nome
      t.string :email
      t.string :usuario
      t.string :senha

      t.timestamps null: false
    end
  end
end
