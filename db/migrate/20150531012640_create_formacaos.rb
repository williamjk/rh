class CreateFormacaos < ActiveRecord::Migration
  def change
    create_table :formacaos do |t|
      t.integer :idCliente
      t.string :nomeInstituicao
      t.string :titulo
      t.date :dtInicio
      t.date :dtFinal

      t.timestamps null: false
    end
  end
end
