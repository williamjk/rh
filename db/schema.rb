# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150531012944) do

  create_table "agencia", force: :cascade do |t|
    t.string   "nome",               limit: 255
    t.string   "nomadministrador",   limit: 255
    t.string   "emailadministrador", limit: 255
    t.string   "status",             limit: 255
    t.integer  "tipocontaagencia",   limit: 4
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  create_table "clientes", force: :cascade do |t|
    t.string   "nome",       limit: 255
    t.string   "email",      limit: 255
    t.date     "dtNasc"
    t.text     "resumo",     limit: 65535
    t.string   "titulo",     limit: 255
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "conta_agencia", force: :cascade do |t|
    t.float    "valor",      limit: 24
    t.string   "formaPgto",  limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "conta_empresas", force: :cascade do |t|
    t.float    "valor",      limit: 24
    t.string   "formaPgto",  limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "cursos", force: :cascade do |t|
    t.integer  "idCliente",       limit: 4
    t.string   "nomeInstituicao", limit: 255
    t.string   "titulo",          limit: 255
    t.date     "dtInicio"
    t.date     "dtFinal"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "empresas", force: :cascade do |t|
    t.string   "nome",               limit: 255
    t.text     "descricao",          limit: 65535
    t.string   "site",               limit: 255
    t.text     "endereco",           limit: 65535
    t.string   "tamanho",            limit: 255
    t.string   "nmresponsavelrh",    limit: 255
    t.string   "emailresponsavelrh", limit: 255
    t.integer  "tipocontaempresa",   limit: 4
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  create_table "experiencia", force: :cascade do |t|
    t.integer  "idCliente",  limit: 4
    t.string   "cargo",      limit: 255
    t.integer  "empresa",    limit: 4
    t.date     "dtInicial"
    t.date     "dtFinal"
    t.text     "descricao",  limit: 65535
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "feedbacks", force: :cascade do |t|
    t.integer  "idcliente",  limit: 4
    t.integer  "idusuario",  limit: 4
    t.text     "descricao",  limit: 65535
    t.integer  "nota",       limit: 4
    t.integer  "idvaga",     limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "formacaos", force: :cascade do |t|
    t.integer  "idCliente",       limit: 4
    t.string   "nomeInstituicao", limit: 255
    t.string   "titulo",          limit: 255
    t.date     "dtInicio"
    t.date     "dtFinal"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "idiomas", force: :cascade do |t|
    t.integer  "idCliente",       limit: 4
    t.string   "nomeInstituicao", limit: 255
    t.string   "titulo",          limit: 255
    t.string   "nivel",           limit: 255
    t.date     "dtInicio"
    t.date     "dtFinal"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "temporarios", force: :cascade do |t|
    t.integer  "idvaga",         limit: 4
    t.integer  "idcliente",      limit: 4
    t.string   "inscrito",       limit: 255
    t.string   "preselecionado", limit: 255
    t.string   "efetivado",      limit: 255
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "usuarios", force: :cascade do |t|
    t.integer  "agencia",    limit: 4
    t.integer  "empresa",    limit: 4
    t.string   "nome",       limit: 255
    t.string   "email",      limit: 255
    t.string   "usuario",    limit: 255
    t.string   "senha",      limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "vagas", force: :cascade do |t|
    t.string   "titulo",     limit: 255
    t.text     "descricao",  limit: 65535
    t.integer  "empresa",    limit: 4
    t.date     "dtinicial"
    t.date     "dtfinal"
    t.string   "status",     limit: 255
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

end
