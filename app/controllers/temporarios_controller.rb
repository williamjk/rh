class TemporariosController < ApplicationController
  before_action :set_temporario, only: [:show, :edit, :update, :destroy]

  # GET /temporarios
  # GET /temporarios.json
  def index
    @temporarios = Temporario.all
  end

  # GET /temporarios/1
  # GET /temporarios/1.json
  def show
  end

  # GET /temporarios/new
  def new
    @temporario = Temporario.new
  end

  # GET /temporarios/1/edit
  def edit
  end

  # POST /temporarios
  # POST /temporarios.json
  def create
    @temporario = Temporario.new(temporario_params)

    respond_to do |format|
      if @temporario.save
        format.html { redirect_to @temporario, notice: 'Temporario was successfully created.' }
        format.json { render :show, status: :created, location: @temporario }
      else
        format.html { render :new }
        format.json { render json: @temporario.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /temporarios/1
  # PATCH/PUT /temporarios/1.json
  def update
    respond_to do |format|
      if @temporario.update(temporario_params)
        format.html { redirect_to @temporario, notice: 'Temporario was successfully updated.' }
        format.json { render :show, status: :ok, location: @temporario }
      else
        format.html { render :edit }
        format.json { render json: @temporario.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /temporarios/1
  # DELETE /temporarios/1.json
  def destroy
    @temporario.destroy
    respond_to do |format|
      format.html { redirect_to temporarios_url, notice: 'Temporario was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_temporario
      @temporario = Temporario.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def temporario_params
      params.require(:temporario).permit(:idvaga, :idcliente, :inscrito, :preselecionado, :efetivado)
    end
end
