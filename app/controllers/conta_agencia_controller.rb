class ContaAgenciaController < ApplicationController
  before_action :set_conta_agencium, only: [:show, :edit, :update, :destroy]

  # GET /conta_agencia
  # GET /conta_agencia.json
  def index
    @conta_agencia = ContaAgencium.all
  end

  # GET /conta_agencia/1
  # GET /conta_agencia/1.json
  def show
  end

  # GET /conta_agencia/new
  def new
    @conta_agencium = ContaAgencium.new
  end

  # GET /conta_agencia/1/edit
  def edit
  end

  # POST /conta_agencia
  # POST /conta_agencia.json
  def create
    @conta_agencium = ContaAgencium.new(conta_agencium_params)

    respond_to do |format|
      if @conta_agencium.save
        format.html { redirect_to @conta_agencium, notice: 'Conta agencium was successfully created.' }
        format.json { render :show, status: :created, location: @conta_agencium }
      else
        format.html { render :new }
        format.json { render json: @conta_agencium.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /conta_agencia/1
  # PATCH/PUT /conta_agencia/1.json
  def update
    respond_to do |format|
      if @conta_agencium.update(conta_agencium_params)
        format.html { redirect_to @conta_agencium, notice: 'Conta agencium was successfully updated.' }
        format.json { render :show, status: :ok, location: @conta_agencium }
      else
        format.html { render :edit }
        format.json { render json: @conta_agencium.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /conta_agencia/1
  # DELETE /conta_agencia/1.json
  def destroy
    @conta_agencium.destroy
    respond_to do |format|
      format.html { redirect_to conta_agencia_url, notice: 'Conta agencium was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_conta_agencium
      @conta_agencium = ContaAgencium.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def conta_agencium_params
      params.require(:conta_agencium).permit(:valor, :formaPgto)
    end
end
