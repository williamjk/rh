class ContaEmpresasController < ApplicationController
  before_action :set_conta_empresa, only: [:show, :edit, :update, :destroy]

  # GET /conta_empresas
  # GET /conta_empresas.json
  def index
    @conta_empresas = ContaEmpresa.all
  end

  # GET /conta_empresas/1
  # GET /conta_empresas/1.json
  def show
  end

  # GET /conta_empresas/new
  def new
    @conta_empresa = ContaEmpresa.new
  end

  # GET /conta_empresas/1/edit
  def edit
  end

  # POST /conta_empresas
  # POST /conta_empresas.json
  def create
    @conta_empresa = ContaEmpresa.new(conta_empresa_params)

    respond_to do |format|
      if @conta_empresa.save
        format.html { redirect_to @conta_empresa, notice: 'Conta empresa was successfully created.' }
        format.json { render :show, status: :created, location: @conta_empresa }
      else
        format.html { render :new }
        format.json { render json: @conta_empresa.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /conta_empresas/1
  # PATCH/PUT /conta_empresas/1.json
  def update
    respond_to do |format|
      if @conta_empresa.update(conta_empresa_params)
        format.html { redirect_to @conta_empresa, notice: 'Conta empresa was successfully updated.' }
        format.json { render :show, status: :ok, location: @conta_empresa }
      else
        format.html { render :edit }
        format.json { render json: @conta_empresa.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /conta_empresas/1
  # DELETE /conta_empresas/1.json
  def destroy
    @conta_empresa.destroy
    respond_to do |format|
      format.html { redirect_to conta_empresas_url, notice: 'Conta empresa was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_conta_empresa
      @conta_empresa = ContaEmpresa.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def conta_empresa_params
      params.require(:conta_empresa).permit(:valor, :formaPgto)
    end
end
