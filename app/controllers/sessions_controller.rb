class SessionsController < ApplicationController
  def new
  end

  def create
    usuario = Usuario.find_by(email: params[:session][:email].downcase)
    if usuario && usuario.authenticate(params[:session][:senha])
      # Log the user in and redirect to the user's show page.
    else
       #flash[:danger] = 'Inválido email/password.' # Not quite right!
      render 'new'
    end
  end

  def destroy
  end
end
