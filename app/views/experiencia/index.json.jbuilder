json.array!(@experiencia) do |experiencium|
  json.extract! experiencium, :id, :idCliente, :cargo, :empresa, :dtInicial, :dtFinal, :descricao
  json.url experiencium_url(experiencium, format: :json)
end
