json.array!(@agencia) do |agencium|
  json.extract! agencium, :id, :nome, :nomadministrador, :emailadministrador, :status, :tipocontaagencia
  json.url agencium_url(agencium, format: :json)
end
