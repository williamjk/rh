json.array!(@vagas) do |vaga|
  json.extract! vaga, :id, :titulo, :descricao, :empresa, :dtinicial, :dtfinal, :status
  json.url vaga_url(vaga, format: :json)
end
