json.array!(@feedbacks) do |feedback|
  json.extract! feedback, :id, :idcliente, :idusuario, :descricao, :nota, :idvaga
  json.url feedback_url(feedback, format: :json)
end
