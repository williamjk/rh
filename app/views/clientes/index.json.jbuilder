json.array!(@clientes) do |cliente|
  json.extract! cliente, :id, :nome, :email, :dtNasc, :resumo, :titulo
  json.url cliente_url(cliente, format: :json)
end
