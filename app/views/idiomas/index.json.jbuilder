json.array!(@idiomas) do |idioma|
  json.extract! idioma, :id, :idCliente, :nomeInstituicao, :titulo, :nivel, :dtInicio, :dtFinal
  json.url idioma_url(idioma, format: :json)
end
