json.array!(@formacaos) do |formacao|
  json.extract! formacao, :id, :idCliente, :nomeInstituicao, :titulo, :dtInicio, :dtFinal
  json.url formacao_url(formacao, format: :json)
end
