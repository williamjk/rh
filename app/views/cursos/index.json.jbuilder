json.array!(@cursos) do |curso|
  json.extract! curso, :id, :idCliente, :nomeInstituicao, :titulo, :dtInicio, :dtFinal
  json.url curso_url(curso, format: :json)
end
