json.array!(@conta_empresas) do |conta_empresa|
  json.extract! conta_empresa, :id, :valor, :formaPgto
  json.url conta_empresa_url(conta_empresa, format: :json)
end
