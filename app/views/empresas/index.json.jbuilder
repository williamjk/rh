json.array!(@empresas) do |empresa|
  json.extract! empresa, :id, :nome, :descricao, :site, :endereco, :tamanho, :nmresponsavelrh, :emailresponsavelrh, :tipocontaempresa
  json.url empresa_url(empresa, format: :json)
end
