json.array!(@conta_agencia) do |conta_agencium|
  json.extract! conta_agencium, :id, :valor, :formaPgto
  json.url conta_agencium_url(conta_agencium, format: :json)
end
