json.array!(@usuarios) do |usuario|
  json.extract! usuario, :id, :agencia, :empresa, :nome, :email, :usuario, :senha
  json.url usuario_url(usuario, format: :json)
end
