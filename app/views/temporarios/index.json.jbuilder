json.array!(@temporarios) do |temporario|
  json.extract! temporario, :id, :idvaga, :idcliente, :inscrito, :preselecionado, :efetivado
  json.url temporario_url(temporario, format: :json)
end
