require 'test_helper'

class ContaAgenciaControllerTest < ActionController::TestCase
  setup do
    @conta_agencium = conta_agencia(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:conta_agencia)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create conta_agencium" do
    assert_difference('ContaAgencium.count') do
      post :create, conta_agencium: { formaPgto: @conta_agencium.formaPgto, valor: @conta_agencium.valor }
    end

    assert_redirected_to conta_agencium_path(assigns(:conta_agencium))
  end

  test "should show conta_agencium" do
    get :show, id: @conta_agencium
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @conta_agencium
    assert_response :success
  end

  test "should update conta_agencium" do
    patch :update, id: @conta_agencium, conta_agencium: { formaPgto: @conta_agencium.formaPgto, valor: @conta_agencium.valor }
    assert_redirected_to conta_agencium_path(assigns(:conta_agencium))
  end

  test "should destroy conta_agencium" do
    assert_difference('ContaAgencium.count', -1) do
      delete :destroy, id: @conta_agencium
    end

    assert_redirected_to conta_agencia_path
  end
end
