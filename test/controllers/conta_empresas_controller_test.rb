require 'test_helper'

class ContaEmpresasControllerTest < ActionController::TestCase
  setup do
    @conta_empresa = conta_empresas(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:conta_empresas)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create conta_empresa" do
    assert_difference('ContaEmpresa.count') do
      post :create, conta_empresa: { formaPgto: @conta_empresa.formaPgto, valor: @conta_empresa.valor }
    end

    assert_redirected_to conta_empresa_path(assigns(:conta_empresa))
  end

  test "should show conta_empresa" do
    get :show, id: @conta_empresa
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @conta_empresa
    assert_response :success
  end

  test "should update conta_empresa" do
    patch :update, id: @conta_empresa, conta_empresa: { formaPgto: @conta_empresa.formaPgto, valor: @conta_empresa.valor }
    assert_redirected_to conta_empresa_path(assigns(:conta_empresa))
  end

  test "should destroy conta_empresa" do
    assert_difference('ContaEmpresa.count', -1) do
      delete :destroy, id: @conta_empresa
    end

    assert_redirected_to conta_empresas_path
  end
end
