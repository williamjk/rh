require 'test_helper'

class TemporariosControllerTest < ActionController::TestCase
  setup do
    @temporario = temporarios(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:temporarios)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create temporario" do
    assert_difference('Temporario.count') do
      post :create, temporario: { efetivado: @temporario.efetivado, idcliente: @temporario.idcliente, idvaga: @temporario.idvaga, inscrito: @temporario.inscrito, preselecionado: @temporario.preselecionado }
    end

    assert_redirected_to temporario_path(assigns(:temporario))
  end

  test "should show temporario" do
    get :show, id: @temporario
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @temporario
    assert_response :success
  end

  test "should update temporario" do
    patch :update, id: @temporario, temporario: { efetivado: @temporario.efetivado, idcliente: @temporario.idcliente, idvaga: @temporario.idvaga, inscrito: @temporario.inscrito, preselecionado: @temporario.preselecionado }
    assert_redirected_to temporario_path(assigns(:temporario))
  end

  test "should destroy temporario" do
    assert_difference('Temporario.count', -1) do
      delete :destroy, id: @temporario
    end

    assert_redirected_to temporarios_path
  end
end
